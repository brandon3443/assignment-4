/*NAME/Date/File
Name:	Brandon Miller and Phong Pham
Date:	11/22/2015
File:	Assignment4.java
*/
/*ANALYSIS
Input:

Output:

Test Data:
day			date	workTime	scrnTime

monday 		23		8.0			8.0			8.0/24*100 -> 33.33% working and 08.00/24*100 -> 33.33% screen time
tuesday 	24		5.0			9.0			5.0/24*100 -> 20.83% working and 09.00/24*100 -> 37.50% screen time
wednesday 	25		8.5			7.5			8.0/24*100 -> 35.41% working and 08.00/24*100 -> 33.33% screen time
thursday 	26		6.0			10.0		6.0/24*100 -> 25.00% working and 10.00/24*100 -> 41.66% screen time
friday 		27		5.0			10.0		5.0/24*100 -> 20.83% working and 10.00/24*100 -> 41.66% screen time
saturday 	28		2.0			12.5		2.0/24*100 -> 08.33% working and 12.50/24*100 -> 52.08% screen time
sunday 		29		2.5			12.0		2.5/24*100 -> 10.41% working and 12.00/24*100 -> 50.00% screen time

ROUTINE:

high:	35.41% working		wednesday
		52.08% scrnTime		saturday
		
low:	10.41% working		saturday
		33.33% scrnTime		wednesday

avg:	(33.33+37.50+33.33+41.66+41.66+50.00+50.00)/7 = 41.06% of the day spent on scrnTime
		(33.33+20.83+35.41+25.00+20.83+08.33+10.41)/7 = 21.72% of the day speant working

numRecords: 7

total:	24*7 = 168
		(8+5+8+6+5+2+2.5)		= 36.5 working
		(8+9+8+10+10+12.5+12)	= 69.5 scrnTime


Algorithm:

MAIN:

initialize variables
P+G outfile name/path
call getInfile

while(line!=null)
	populate array


	for(nextfield.length)

Method Name:		getInFile
Return Value: 		file
Input Parameters: 	none
Output Parameters:	none
Functionality: 		get infile and path name

	
call printDispDate
call calcPercentWork
call calcPercentScrn
call calcHighWork
call calcLowWork
call calcHighScrn
call calcLowScrn
call highStringWork
call lowStringWork
call highStringScrn
call call lowStringScrn
call calcTotalWork
call calcTotalScreen
call calcAvgWork
call calcAvgScrn
	
call printDispTitles
call printDispArray
	
call printDispEverything
	switch partime/full time
		partime
			if thresholds
				print suggestions
				
			
		fulltime
			if thresholds
				print suggestions

}end main

Method Name:		getInFile
Return Value: 		file
Input Parameters: 	none
Output Parameters:	none
Functionality: 		get infile and path name

Method Name:		printDispDate
Return Value: 		void
Input Parameters: 	UtilityClass outfile
Output Parameters:	none
Functionality: 		print and display current date, company name, programmer's names from UtilityClass

Method Name: 		printDispTitles
Return Value: 		void
Input Parameters:	UtilityClass outfile
Output Parameters:	none
Functionality: 		print and display titles for columns for data records

Method Name:		printDispEverything
Return Value: 		void
Input Parameters:	highScrn,highStringScrn,highWork,highStringWork, totalScrnTime,totalWorkTime,avgWork,avgScrn,UtilityClass outfile
Output Parameters:	none
Functionality: 		print and display the high and low percentages (for workTime and scrnTime),
					numRecords, total scrnTime, total workTime, avgWork, avgScrn

Method Name: 		calcPercentWork
Return Value:		workPercent
Input Parameters:	int i, workTime[]
Output Parameters:	none
Functionality:		calculates the percentage of the day spent working, and returns that value

Method Name:		calcPercentScrn
Return Value:		scrnPercent
Input Parameters:	int i, scrnTime[]
Output Parameters:	none
Functionality:		calculates the percentage of the day spent on screen time, returns that value

Method Name:		calcHighWork
Return Value:		highWork
Input Parameters:	int i, workTime[]
Output Parameters:	none
Functionality:		calculates the highest value of workPercent and returns it

Method Name:		calcLowWork
Return Value:		lowWork
Input Parameters:	int i, workTime[]
Output Parameters:	none
Functionality:		calculates the lowest value of workPercent and returns it

Method Name:		calcHighScrn
Return Value:		highScrn
Input Parameters:	int i, scrnTime[]
Output Parameters:	none
Functionality:		calculates the highest value of scrnPercent and returns it

Method Name:		calcLowScrn
Return Value:		lowScrn
Input Parameters:	int i, scrnTime[]
Output Parameters:	none
Functionality:		calculates the lowest value of scrnPercent and returns it

Method Name:		highStringWorkMatch
Return Value:		highStringWork
Input Parameters:	int i, dayArray[], workTime[], highWork
Output Parameters:	none
Functionality:		matches the highString to highWork, and returns it

Method Name:		lowStringWorkMatch
Return Value:		lowStringWork
Input Parameters:	int i, dayArray[], workTime[], lowWork
Output Parameters:	none
Functionality:		matches the lowString to lowWork, and returns it

Method Name:		highStringScrnMatch
Return Value:		highStringScrn
Input Parameters:	int i, dayArray[], scrnTime[], highScrn
Output Parameters:	matches highString to highScrn
Functionality:

Method Name:		lowStringScrnMatch
Return Value:		lowStringScrn
Input Parameters:	int i, dayArray[], scrnTime, lowScrn
Output Parameters:	matches lowString to lowScrn
Functionality:		

Method Name:		calcTotalScreen
Return Value:		totalScreen
Input Parameters:	int i, scrnTime[],totalScrnTime
Output Parameters:	none
Functionality:		calculates the total screen time and returns it

Method Name:		calcTotalWork
Return Value:		totalWork
Input Parameters:	int i, workTime, totalWorkTime
Output Parameters:	none
Functionality:		calculates the total work time and returns it

Method Name:		calcAvgScrn
Return Value:		avgScrn
Input Parameters:	totalScrnTime, numRecords
Output Parameters:	none
Functionality:		calculates the average screen time and returns it

Method Name:		calcAvgWork
Return Value:		avgWork
Input Parameters:	totalWorkTime, numRecords
Output Parameters:	none
Functionality:		calculates the average work time and returns it

Method Name:		printDispArray
Return Value: 		void
Input Parameters: 	UtilityClass,int i,dayArray[],dateArray[], workTime[],scrnTime[],
Output Parameters:	none
Functionality: 		print and display arrays

*/

import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
public class assignment4
{
	static final int MAX_LINES = 10;
	static Scanner sc = new Scanner(System.in);
	public static void main(String[]args)throws IOException
	{
		int i = 0;
		int numRecords = 0;
		int [] dateArray = new int [MAX_LINES];
		double highScrn = 0;
		double highWork = 0;
		double lowWork = 25;
		double lowScrn = 25;
		double totalScrnTime = 0;
		double totalWorkTime = 0;
		double avgWork = 0;
		double avgScrn = 0;
		double[] workTime = new double[MAX_LINES];
		double[] scrnTime = new double [MAX_LINES];
		double[] workPercent = new double [MAX_LINES];
		double[] scrnPercent = new double [MAX_LINES];
		String [] dayArray = new String [MAX_LINES];
		String file;
		String Out;
		String highStringScrnMatch;
		String highStringScrn;
		String lowStringScrn;
		String lowStringScrnMatch;
		String line;
		System.out.println("Please enter the path and file to which you wish to save the output:");
		Out = sc.nextLine();
		UtilityClass outfile = new UtilityClass(Out);   	//create a new instance of UtilityClass
		outfile.openFile(); 								//call to method openFile from UtilityClass
		file = getInFile();
		UtilityClassIn infile = new UtilityClassIn(file);
		infile.openInFile();
		Scanner inputFile= new Scanner(file);
		/*if(inputFile.hasNext()) // checks if first line is blank
		{
			inputFile.nextLine();
		}*/
		//String line=infile.readLineFromFile();  
		line = inputFile.nextLine();
		printDispTitles(outfile);
		while(inputFile.hasNext())           //loop condition;  null means value is unknown, therefore while there is something on the line
		{             
				line = inputFile.nextLine();
				System.out.println("Call");
			String[] nextfield=line.split(",");
			dayArray[i] = (nextfield[0]);
			dateArray[i] = Integer.parseInt(nextfield[1]);
			workTime[i] = Double.parseDouble(nextfield[2]);
			scrnTime[i] = Double.parseDouble(nextfield[3]);
			
		}
		
		System.out.print(dateArray[3]);
		outfile.closeFile();
	}//end main

	public static String getInFile()
	{
		String infile;
		System.out.println("Please enter the name/path for the infile:");
		infile=sc.nextLine();
		return infile;
	}
	public static void printDispDate(UtilityClass outfile, UtilityClass date, UtilityClass name)
	{
		String date = outfile.myDate();
		System.out.println(date);
		outfile.writeLineToFile(date);
		String name = outfile.myName();
		System.out.println(name);
		outfile.writeLineToFile(name);
		System.out.println("Company Name: NaCl Consulting\n")
	}
	public static void printDispTitles(UtilityClass outfile)
	{
		System.out.println("\nDAY\t\tDATE\tWORK TIME\tSCREEN TIME\n");
		outfile.writeLineToFile("\nDAY\tDATE\tWORK TIME\t\tSCREEN TIME\n");
	}
/*	public static void printDispEverything(highScrn,highStringScrn,highWork,highStringWork,lowScrn,lowStringScrn,lowWork,lowStringWork,totalScrnTime,totalWorkTime,avgScrn,avgWork,UtilityClass outfile)
	{
		System.out.printf("The highest time spent on screen time was %.2f and was on a %s\nThe lowest time spent on screen time was $.2f and was on a %s\nThe highest time spent on work was %.2f and was on a %s\nThe lowest time spent on work was %.2f and was on a %s\n\nThe total time spent on\nWORK: %.2f\nSCREEN TIME: %.2f\n\nThe Average time spent on:\nWORK: %.2f\nSCREEN TIME: %.2f");
		outfile.writeLineToFile("The highest time spent on screen time was %.2f and was on a %s\nThe lowest time spent on screen time was $.2f and was on a %s\nThe highest time spent on work was %.2f and was on a %s\nThe lowest time spent on work was %.2f and was on a %s\n\nThe total time spent on\nWORK: %.2f\nSCREEN TIME: %.2f\n\nThe Average time spent on:\nWORK: %.2f\nSCREEN TIME: %.2f");
<<<<<<< HEAD
	}*/

=======
	}
	public static double calcPercentWork(int i, double workTime[],double workPercent[])
	{
		for(i=0;i>workTime[].length();i++)
		{
			workPercent[i] = ((workTime[i]/24)*100);
		}
	}
	public static double calcPercentScrn(int i,double scrnTime[],double scrnPercent[])
	{
		for(i=0;i>scrnTime.length;i++)
		{
			scrnPercent[i] = ((scrnTime[i]/24)*100);
			return scrnPercent[i];
		}
	}
	public static double calcHighWork(double workTime[],int i,highWork)
	{
		if(workTime[i]>highWork)
		{
			highWork=workTime[i];
		}
		return highWork;
	}
	public static double calcHighScrn(double scrnTime[],int i,highScrn)
	{
		if(scrnTime[i]>highScrn)
		{
			highScrn=scrnTime[i];
		}
		return highScrn;
	}
	public static double calcLowWork(double workTime[],int i,lowWork)
	{
		if(workTime[i]<lowWork)
		{
			lowWork=workTime[i];
		}
		return lowWork;
	}
	public static double calcLowScrn(double scrnTime[],int,lowScrn)
	{
		if(scrnTime[i]<lowScrn)
		{
			lowScrn = scrnTime[];
		}
		return lowScrn;
	}
	public static String highStringWorkMatch(int i,highWork,dayArray[],workTime[])
	{
		if(workTime[i]==highWork)
		{
			highStringWork=dayArray[i];
		}
		return highStringWork;
	}
	public static String lowStringWorkMatch(int i,lowWork,dayArray[],workTime[])
	{
		if(workTime[i]==lowWork)
		{
			lowStringWork=dayArray[i];
		}
		return lowStringWork;
	}
	public static String highStringScrnMatch(int i, highScrn,dayArray[],scrnTime[])
	{
		if(scrnTime[i]==highScrn)
		{
			highStringScrn=dayArray[i];
		}
		return highStringScrn;
	}
	public static String lowStringScrnMatch(int i, lowScrn,dayArray[],scrnTime[])
	{
		if(scrnTime[i]==lowScrn)
		{
			lowStringScrn=dayArray[i];
		}
		return lowStringScrn;
	}
	public static double calcTotalScreen(int i, scrnTime[],totalScrnTime)
	{
		for(i=0;i>scrnTime.length;i++)
		{
			totalScrnTime+=scrnTime[i];
		}
		return totalScrnTime;
	}
	public static double calcTotalWork(int i, workTime[],totalWorkTime)
	{
		for(i=0;i>workTime.length;i++)
		{
			totalWorkTime+=workTime[i];
		}
		return totalWorkTime;
	}
	public static double calcAvgScrn(totalScrnTime, numRecords)
	{
		avgScrn=(totalScrnTime/numRecords);
		return avgScrn;
	}
	public static double calcAvgWork(totalScrnTime,numRecords)
	{
		avgScrn=(totalWorkTime/numRecords);
		return avgWork;
	}
	public static void printDispArray(UtilityClass outfile, int i,dateArray[],dayArray[],workTime[],scrnTime[])
	{
		for(i=0;i>dateArray.length;i++)
		{
			System.out.printf("%s\t\t%d\t%.2f\t%.2f\n",dayArray[i],dateArray[i],workTime[i],scrnTime[i]);
		}
	}
>>>>>>> 2efe6a2552db3ed6417c99ab080d4168b47651e8
}//end class

